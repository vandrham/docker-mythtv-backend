# README #

This is a docker image for running a MythTV backend server including database.
It is based on the mythtv-backend image created by apnar.
Key differences are:

 * This image is based on ubuntu 18.04
 * Running mythtv-setup is enabled by  X11 forwarding


### Quick start ###

* Create a directory on your host where the database will be stored
* Create a directory on your host where the recordings will be stored
* Create a bridge network with a subnet that does not overlap with your host's subnet

```
docker network create --driver=bridge \
  -o "com.docker.network.bridge.name=mynet"
  -o parent=eth0 \
  --subnet=192.168.2.0/24
  mynet
```

* If you want to be able to access the mythtv-backend from other machines in the host network, add two iptables rules

```sudo iptables -A FORWARD -i eth0 -o mynet -j ACCEPT```
```sudo iptables -A FORWARD -i mynet -o eth0 -j ACCEPT```

* Start a new docker container

```
docker run -d
  --name=mythtv-backend \
  --net=mynet \
  -h mythtv-backend \
  -v /etc/localtime:/etc/localtime:ro \
  -v <database directory>:/var/lib/mysql \
  -v <recordings directory>:/mnt/recordings \
  vandrham/mythtv-backend
```

If no database is present in the database directory, a new one will be created. This may take a few seconds.
Inspect the output of ```docker logs mythtv-backend``` to see if the inital setup has finished.

Once the container is started, mythtv-setup can be started with the following command:

```ssh -X mythtv@192.168.2.2 /usr/bin/mythtv-setup```

The password for the mythtv user is set to ```mythtv```

In mythtv-setup 'General / Host Address Backend Setup', make sure to enable both 'Allow Connections from all Subnets' and 'Listen on All IP Addresses'.
These settings are needed if you want to connect a mythtv-frontend running outside of the docker network