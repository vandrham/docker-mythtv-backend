#!/bin/bash
if [ ! -f /.root_pw_set ]; then
	/set_root_pw.sh
fi

if [ ! -z "${MYSQL_HOST}" ]; then
  sed -i "s#<Host>.*#<Host>${MYSQL_HOST}</Host>#" /etc/mythtv/config.xml
else
  MYSQL_HOST=localhost
fi

if [ ! -z "${MYSQL_USER}" ]; then
  sed -i "s#<UserName>.*#<UserName>${MYSQL_USER}</UserName>#" /etc/mythtv/config.xml
else
  MYSQL_USER=mythtv
fi

if [ ! -z "${MYSQL_PASS}" ]; then
  sed -i "s#<Password>.*#<Password>${MYSQL_PASS}</Password>#" /etc/mythtv/config.xml
else
  MYSQL_PASS=mythtv
fi

if [ ! -z "${MYSQL_DATABASE}" ]; then
  sed -i "s#<DatabaseName>.*#<DatabaseName>${MYSQL_DATABASE}</DatabaseName>#" /etc/mythtv/config.xml
else
  MYSQL_DATABASE=mythconverg
fi

if [ ! -z "${MYSQL_PORT}" ]; then
  sed -i "s#<Port>.*#<Port>${MYSQL_PORT}</Port>#" /etc/mythtv/config.xml
fi

chown -R mysql:mysql /var/run/mysqld 
DIR="/var/lib/mysql"
if [ "$(ls -A $DIR)" ]; then
    echo "$DIR is not Empty - skipping database initialization"
    chown -R mysql:mysql /var/lib/mysql
else
    echo "$DIR is Empty - initializing mariadb"
    chown -R mysql:mysql /var/lib/mysql
    /usr/bin/mysql_install_db --user=mysql
    cd '/usr' ; /usr/bin/mysqld_safe --datadir='/var/lib/mysql' &
    sleep 3
    /usr/sbin/dpkg-reconfigure mythtv-database
    mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root mysql
    mysql -u root -e "GRANT ALL ON ${MYSQL_DATABASE}.* TO '${MYSQL_USER}'@'localhost' IDENTIFIED BY '${MYSQL_PASS}';"
    mysql -u root -e "SET password for '${MYSQL_USER}'@'${MYSQL_HOST}' = password('${MYSQL_PASS}');"
    /usr/bin/mysqladmin shutdown
    chown -R mythtv:users /mnt/recordings /mnt/movies
    echo "db initialized"
fi

echo "starting supervisord"
exec /usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf 1>/dev/null
